@extends('app')

@section('content')
    <h1>Articles</h1>
    <hr>
    @foreach($articles as $article)
        <article>
            <h2>
                {{--<a href="/article/{{$article->id}}"> {{$article->title}}</a>--}}
                {{--<a href="{{action('ArticleController@show',[$article->id])}}"> {{$article->title}}</a>--}}
                <a href="{{url('/article',$article->id)}}"> {{$article->title}}</a>
            </h2>
            <div class="body">{{$article->body}}</div>
        </article>
    @endforeach


@stop