<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Log;

Route::get('/', function () {
    return view('welcome');
});

Route::resources([
    'article'=>'ArticleController',
 ]);

Route::get('foo', function () {
    Log::error("foo route called");
    return 'Hello World';
});

Route::get('/test', function () {
    return view('test');
});

Route::get('/bar', function () {
    Log::error("bar route called");
    return 'Hello World';
});

Route::get('/dbexp', function () {
    Log::error("bar route called");
    return 'Hello World';
});




